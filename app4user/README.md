## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Fri Apr 16 2021 12:06:55 GMT+0800 (중국 표준시)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.1.7|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>simple|
|**Service Type**<br>OData Url|
|**Service URL**<br>http://hana22.aspn.com.cn:8001/sap/opu/odata/SAP/ZSDB_USER_SRV/
|**Module Name**<br>app4user|
|**Application Title**<br>User Info CRUD|
|**Namespace**<br>sapui5.crud|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|

## app4user

User Info CRUD

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```


#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


