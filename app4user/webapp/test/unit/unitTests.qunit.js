/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"sapui5crud./app4user/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
