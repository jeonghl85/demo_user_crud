sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/format/NumberFormat"
], function (JSONModel, NumberFormat) {
    "use strict";
    return {
        currStatus: function (sStatus) {

            var iCurr;

            var oModel = new JSONModel();
            oModel = this.getView().getModel("readUser");
            // var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
            //     pattern: "yyyy-MM-dd"
            // });

            // var dateFormatted = dateFormat.format(oModel.getProperty("/Brith"));

            // return dateFormatted;

            var oCurrencyFormat = NumberFormat.getCurrencyInstance({
                currencyCode: true
            });

            debugger;

            iCurr = oCurrencyFormat.format(oModel.getProperty("/Salary"), oModel.getProperty("/Waers"));

            return iCurr;
        }
    };
});