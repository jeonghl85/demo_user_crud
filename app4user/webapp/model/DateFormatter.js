sap.ui.define([
    "sap/ui/model/json/JSONModel"
], function (JSONModel) {
    "use strict";
    return {
        dateStatus: function (sStatus) {

            var oModel = new JSONModel();
            oModel = this.getView().getModel("readUser");
            var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
                pattern: "yyyy-MM-dd"
            });

            var dateFormatted = dateFormat.format(oModel.getProperty("/Brith"));

            return dateFormatted;
        }
    };
});