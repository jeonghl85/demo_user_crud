sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"sapui5/crud/app4user/model/models",
	"./controller/HelpDialog"
], function (UIComponent, Device, models, HelpDialog) {
	"use strict";

	return UIComponent.extend("sapui5.crud.app4user.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			// set dialog
			this._helpDialog = new HelpDialog(this.getRootControl());
		},

		exit: function () {
			this._helpDialog.destroy();
			delete this._helpDialog;
		},

		openHelpDialog: function () {
			this._helpDialog.open();
		}
	});
});