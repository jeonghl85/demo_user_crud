sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/m/MessageBox",
		"sap/ui/model/json/JSONModel",
		"../model/DateFormatter",
		"../model/CurrencyFormatter"
	],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller, MessageBox, JSONModel, DateFormatter, CurrencyFormatter) {
		"use strict";

		var that;

		return Controller.extend("sapui5.crud.app4user.controller.App", {

			DateFormatter: DateFormatter,
			CurrencyFormatter: CurrencyFormatter,

			onInit: function () {

				that = this;

				/* ㅂㅃㅂ*/

			},

			onOpenHelpDialog: function () {
				this.getOwnerComponent().openHelpDialog();
			},

			onAdd: function (params) {
				var userId = this.getView().byId("iUserId").getValue();
				var userName = this.getView().byId("iUserName").getValue();
				var userEmail = this.getView().byId("iEmail").getValue();
				var userMobile = this.getView().byId("iMobile").getValue();
				var userBrith = this.getView().byId("DTP1").getValue() + "T00:00:00"; // 날짜입력시 항상 뒤에 시간을 붙이기.
				var userScore = this.getView().byId("iScore").getValue();
				var userSalary = this.getView().byId("iSalary").getValue();
				var userWaers = this.getView().byId("iWaers").getSelectedItem().getKey();

				var data = {
					Userid: userId,
					Username: userName,
					Email: userEmail,
					Mobile: userMobile,
					Brith: userBrith,
					Score: userScore,
					Salary: userSalary,
					Waers: userWaers
				};

				debugger;

				var odataModel = this.getView().getModel("userInfo");

				odataModel.setHeaders({
					"X-Requested-With": "X"
				});

				odataModel.create("/UserSet", data, {
					success: function (data, response) {
						// debugger;
						MessageBox.success("Data successfully created");
					},
					error: function (error) {
						debugger;

						MessageBox.error("Error while creating the data");
					}
				});

			},

			onEdit: function () {

				var userId = this.getView().byId("iUserId").getValue();
				var userName = this.getView().byId("iUserName").getValue();

				var data = {
					Userid: userId,
					Username: userName
				};

				var path = "/UserSet('" + userId + "')";

				var odataModel = this.getView().getModel("userInfo");

				odataModel.setHeaders({
					"X-Requested-With": "X"
				});

				odataModel.update(path, data, {
					success: function (data, response) {
						MessageBox.success("Successfully Updated");
					},
					error: function (error) {
						MessageBox.error("Error while updating the data");
					}
				});


			},

			onDelete: function () {
				var userId = this.getView().byId("iUserId").getValue();

				var data = {
					Userid: userId
				};

				var path = "/UserSet('" + userId + "')";

				var odataModel = this.getView().getModel("userInfo");

				odataModel.setHeaders({
					"X-Requested-With": "X"
				});

				odataModel.remove(path, {
					success: function (data, response) {
						MessageBox.success("Deleted data");
					},
					error: function (error) {
						MessageBox.error("Deletion failed");
					}
				})
			},

			onRead: function () {

				var userId = this.getView().byId("iUserId").getValue();

				var path = "/UserSet('" + userId + "')";
				var odataModel = this.getView().getModel("userInfo");

				var obj = odataModel.read(path, {
					success: function (oData, oResponse) {

						var oModel = new JSONModel(oData);

						that.getView().setModel(oModel, "readUser");
						that.getView().byId("iUserName").setValue(oModel.getProperty("/Username"));
						// that.getView().byId("DTP1").setValue(oModel.getProperty("/Brith"));

						MessageBox.success("Success");

					},
					error: function (oError) {
						MessageBox.error("Error");
					}
				});
			}


		});
	});
